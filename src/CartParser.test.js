import CartParser from './CartParser';
import { readFileSync } from 'fs';
import * as uuid from 'uuid';

let parser;

jest.mock("fs");
jest.mock("uuid");

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {

	it("should return an error when header field is wrong", () => {
		const csv =
			"Product name,Price,Wrong field\n" +
			"Mollis consequat,9.00,2";

		const expected = [{
			type: "header",
			row: 0,
			column: 2,
			message: "Expected header to be named \"Quantity\" but received Wrong field."
		}]

		expect(parser.validate(csv)).toEqual(expected);
	});

	it("should return an error when header field is missed", () => {
		const csv =
			"Product name,Price\n" +
			"Mollis consequat,9.00,2";

		const expected = [{
			type: "header",
			row: 0,
			column: 2,
			message: "Expected header to be named \"Quantity\" but received undefined."
		}]

		expect(parser.validate(csv)).toEqual(expected);
	});

	it("should return an error when there are less than schema.columns.length cells in the body line", () => {
		const csv =
			"Product name,Price,Quantity\n" +
			"Consectetur";

		const expected = [{
			type: "row",
			row: 1,
			column: -1,
			message: "Expected row to have 3 cells but received 1."
		}];

		expect(parser.validate(csv)).toEqual(expected);
	});

	it("should return no error when there are more than schema.columns.length cells in the body line", () => {
		const csv =
			"Product name,Price,Quantity\n" +
			"Consectetur,28.72,10,EXTRA FIELD";

		const expected = [];

		expect(parser.validate(csv)).toEqual(expected);
	});

	it("should return an error when the cell that must contain the string is empty", () => {
		const csv =
			"Product name,Price,Quantity\n" +
			",9.00,2";

		const expected = [{
			type: "cell",
			row: 1,
			column: 0,
			message: "Expected cell to be a nonempty string but received \"\"."
		}];

		expect(parser.validate(csv)).toEqual(expected);
	})

	it("should return an error when the cell that must contain the number does not contain the number", () => {
		const csv =
			"Product name,Price,Quantity\n" +
			"Tvoluptatem,10.32,STRING";

		const expected = [{
			type: "cell",
			row: 1,
			column: 2,
			message: "Expected cell to be a positive number but received \"STRING\"."
		}];

		expect(parser.validate(csv)).toEqual(expected);
	});

	it("should return an error when the cell that must contain the number contain negative value", () => {
		const csv =
			"Product name,Price,Quantity\n" +
			"Tvoluptatem,10.32,-25";

		const expected = [{
			type: "cell",
			row: 1,
			column: 2,
			message: "Expected cell to be a positive number but received \"-25\"."
		}];

		expect(parser.validate(csv)).toEqual(expected);
	})

	it("should return no error when the body is empty", () => {
		const csv =
			"Product name,Price,Quantity";

		const expected = [];

		expect(parser.validate(csv)).toEqual(expected);
	})

	it("should return no error when the contents of CSV is correct", () => {
		const csv =
			"Product name,Price,Quantity\n" +
			"Mollis consequat,9.00,2";

		const expected = [];

		expect(parser.validate(csv)).toEqual(expected);
	})

	it("should return parsed line", () => {
		const csvLine = "Mollis consequat,9.00,2";

		uuid.v4.mockReturnValueOnce(1);

		const expected = {
			id: 1,
			name: "Mollis consequat",
			price: 9,
			quantity: 2
		}

		expect(parser.parseLine(csvLine)).toEqual(expected);
	});

	it("should return total price of car items", () => {
		const cartItems = [
			{
				id: "3e6def17-5e87-4f27-b6b8-ae78948523a9",
				name: "Mollis consequat",
				price: 9,
				quantity: 2
			},
			{
				id: "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
				name: "Tvoluptatem",
				price: 10.32,
				quantity: 1
			}
		]

		const expected = 28.32;

		expect(parser.calcTotal(cartItems)).toBe(expected);
	})

});

describe('CartParser - integration test', () => {

	it("should return parsed CSV when content is valid", () => {
		const csv =
			"Product name,Price,Quantity\n" +
			"Condimentum aliquet,9,2";

		readFileSync.mockReturnValueOnce(csv);
		uuid.v4.mockReturnValueOnce(1);

		const expected = {
			items: [{
				id: 1,
				name: "Condimentum aliquet",
				price: 9,
				quantity: 2
			}],
			total: 18
		}

		expect(parser.parse("")).toEqual(expected);
	})

});